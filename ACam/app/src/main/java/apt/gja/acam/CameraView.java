package apt.gja.acam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback {  
	private static final String TAG = "CameraView";

	private SurfaceHolder mHolder;
	private Camera mCamera;
	private Camera.Parameters parameters = null;

	private DrawHistogramView histogram = null;
	
	private List<String> exposures = null;
	private int min_exposure = 0;
	private int max_exposure = 0; 
	
	/** Constructors that will allow the View to be instantiated either 
	 * in code or through inflation from a resource layout.
	 */
	public CameraView(Context context){
		super(context);
		Log.d(TAG, "constructor 1");
		initCameraView();
	}
	
	public CameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d(TAG, "constructor 2");
		initCameraView();
	}

	public CameraView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		Log.d(TAG, "constructor 2");
		initCameraView();
	}
	
	@SuppressWarnings("deprecation")
	/** Method initCameraView - set everything important for use of camera view */
	protected void initCameraView(){		
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}
	
	/** Open camera instance for using */
	public void openCamera(){
		try{
			mCamera = Camera.open();
		} catch (Exception e){
			/** ERROR */
		}			
	}
	
	/** Initialize camera parameters */
	private void initCamera() {
		Log.d(TAG, "initCamera starts");
		parameters = mCamera.getParameters();
		
		min_exposure = parameters.getMinExposureCompensation();
		max_exposure = parameters.getMaxExposureCompensation();

		if( min_exposure != 0 || max_exposure != 0 ) {
			exposures = new Vector<String>();
			for(int i = min_exposure; i <= max_exposure; i++) {
				exposures.add("" + i);
			}

			parameters.setExposureCompensation(0);
		}
		
		parameters = mCamera.getParameters();
		histogram = ((MainActivity)this.getContext()).getHistogram();

		Log.d(TAG, "initCamera ends");
	}

	/** Return max supported exposure */
	public int getMaxExposure() {
		return max_exposure;
	}
	
	/** Return minimal supported exposure */
	public int getMinExposure() {
		return min_exposure;
	}
	
	/** Return current exposure setting */
	public int getCurrentExposure() {
		return parameters.getExposureCompensation();
	}
	
	/** Change exposure */
	public void changeExposure(int progress) {
		Log.d(TAG, "change: " + progress);
		if (mCamera != null) {
			int new_exposure = min_exposure + progress;
			parameters.setExposureCompensation(new_exposure);
			mCamera.setParameters(parameters);
		}
	}
	
	/** Return supported resolutions */
	public List<Size> getSupportedResolutions() {
		Camera.Parameters param = mCamera.getParameters();
		return param.getSupportedPictureSizes();
	}

	/** Return supported focus */
	public List<String> getSupportedFocuses() {
		Camera.Parameters param = mCamera.getParameters();
		return param.getSupportedFocusModes();
	}
	
	/** Set camera parameter focus */
	public void setFocus(String str) {
		parameters.setFocusMode(str);
		mCamera.setParameters(parameters);
		parameters = mCamera.getParameters();
	}
	
	/** Return current focus mode */
	public String getCurrentFocusMode() {
		return parameters.getFocusMode();
	}
	
	/** Set camera parameter color effect */
	public void setEffect(String str) {
		parameters.setColorEffect(str);
		mCamera.setParameters(parameters);
		parameters = mCamera.getParameters();
	}
	
	/** Set camera parameter picture size */
	public void setResolution(int w, int h) {
		parameters.setPictureSize(w, h);
		mCamera.setParameters(parameters);
		requestLayout();
		parameters = mCamera.getParameters();
	}

	/** Return camera object */
	public Camera getCameraObject() {
		return mCamera;
	}
	
	/** Set onTouchEvent listener */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.d(TAG, "onTouchEvent");
		int eventaction = event.getAction();

	    switch (eventaction) {
	        case MotionEvent.ACTION_DOWN: 
	            /** focus there */
	        	//Log.d(TAG, "down");
	        	List<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
	        	Rect area = new Rect((int) (event.getX()-100),(int) (event.getY()-100), (int) (event.getX()+100), (int) (event.getY()+100));
	            focusAreas.add(new Camera.Area(area, 100));
	        	parameters.setFocusAreas(focusAreas);
	        	
	            break;

	        case MotionEvent.ACTION_UP:   
	            /** take a picture */
	        	//Log.d(TAG, "up");
	        	MainActivity main_activity = (MainActivity)this.getContext();
	        	Toast.makeText(main_activity.getApplicationContext(), "Take picture", Toast.LENGTH_SHORT).show();
	        	mCamera.takePicture(null, null, main_activity.mPicture);
	            break;
	    }

		return true;
	} 

	/** Set Camera.previewCallback to null */
	public void stopPreviewCallback() {
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
		}
	}

	/** Set Camera.previewCallback */
	public void startPreviewCallback(String color) {
		histogram.color = color;
		if (mCamera != null) {
			mCamera.setPreviewCallback(new PreviewCallback() {
	      	  public void onPreviewFrame(byte[] data, Camera camera)
	      	  {
	      		  /** react for each new frame in CameraView */
	      		  Camera.Parameters params = camera.getParameters();
	      		  
	      		  if (histogram.bitmap == null){
	      			  /** sending data to DrawHistogramView for processing image */
	      			  histogram.imageWidth = params.getPreviewSize().width;
	      			  histogram.imageHeight = params.getPreviewSize().height;
	      			  
	      			  histogram.bitmap = Bitmap.createBitmap(params.getPictureSize().width, params.getPictureSize().height, Bitmap.Config.RGB_565);
	      			  histogram.RGBData = new int[histogram.imageWidth * histogram.imageHeight]; 
	      			  histogram.YUVData = new byte[data.length];
	      			  
	      		  }
	      		  
	      		  /** Send data to DrawHistogramView */
	      		  System.arraycopy(data, 0, histogram.YUVData, 0, data.length);
	      		  histogram.invalidate();
	      		  
	      	  }
			});
		}
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated starts");
		this.openCamera();		
		try {
			mCamera.setPreviewDisplay(holder);
			initCamera();

		} catch (IOException e) {
			Log.i("Error setting camera", e.getMessage());
			mCamera.release();
            mCamera = null;
		}	
		Log.d(TAG, "surfaceCreated ends");
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {		
		Log.d(TAG, "surfaceChanged starts");
        mCamera.startPreview();		
		Log.d(TAG, "surfaceChanged ends");
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed starts");
		if (mCamera != null){
	    	mCamera.setPreviewCallback(null);
	        mCamera.stopPreview();
	        mCamera.release();
	        mCamera = null;
		}
        Log.d(TAG, "surfaceDestroyed ends");
	}

	/** Release camera instance for other applications after to use */
	public void releaseCamera(){
        if (mCamera != null){
        	/** Release the camera for other applications */
            mCamera.release();
            mCamera = null;
        }
    }

}
