package apt.gja.acam;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class DrawHistogramView extends View {
	// private static final String TAG = "DrawHistogramView";

	/** help variable */
	public String color = "RGB";

	/** Paints for histograms */
	private Paint paintBlack;
	private Paint paintRed;
	private Paint paintGreen;
	private Paint paintBlue;

	/** arrays for image */
	public Bitmap bitmap;
	public byte[] YUVData;
	public int[] RGBData;
	public int imageWidth;
	public int imageHeight;

	private int[] histogram;
	private int[] redHistogram;
	private int[] greenHistogram;
	private int[] blueHistogram;

	/**
	 * Constructors which will allow the View to be instantiated either in code or
	 * through inflation from a resource layout.
	 * */
	public DrawHistogramView(Context context) {
		super(context);
		initDrawHistogramView();
	}

	public DrawHistogramView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initDrawHistogramView();
	}

	public DrawHistogramView(Context context, AttributeSet attrs,
			int defaultStyle) {
		super(context, attrs, defaultStyle);
		initDrawHistogramView();
	}

	/** Initialize the control and call it from each constructor. */
	private void initDrawHistogramView() {

		setFocusable(true);

		/** define each Paint (line) */
		paintBlack = new Paint();
		paintBlack.setStyle(Paint.Style.FILL);
		paintBlack.setColor(Color.BLACK);
		paintBlack.setTextSize(25);

		paintRed = new Paint();
		paintRed.setStyle(Paint.Style.FILL);
		paintRed.setColor(Color.RED);
		paintRed.setTextSize(25);

		paintGreen = new Paint();
		paintGreen.setStyle(Paint.Style.FILL);
		paintGreen.setColor(Color.GREEN);
		paintGreen.setTextSize(25);

		paintBlue = new Paint();
		paintBlue.setStyle(Paint.Style.FILL);
		paintBlue.setColor(Color.BLUE);
		paintBlue.setTextSize(25);

		bitmap = null;
		YUVData = null;
		RGBData = null;

		/** 256 value of color */
		histogram = new int[256];
		redHistogram = new int[256];
		greenHistogram = new int[256];
		blueHistogram = new int[256];
		histogram = new int[256];
	}

	@Override
	protected void onDraw(Canvas canvas) {
		/** draw histogram(s) */
		if (bitmap != null) {
			int canvasWidth = canvas.getWidth();
			int canvasHeight = canvas.getHeight();
			int marginWidth = (canvasWidth - canvasWidth) / 2;

			float barMaxHeight = 3000;
			float barWidth = ((float) canvasWidth) / 256;
			float barMarginHeight = 2;
			RectF barRect = new RectF();

			if (color == "gray") {
				/** user wants only grayscale histogram */
				/** convert image from YUV model to grayscale */
				decodeYUV420SPGrayscale(RGBData, YUVData, imageWidth,
						imageHeight);

				/** calculate intensity histogram */
				calculateIntensityHistogram(RGBData, histogram, imageWidth,
						imageHeight, 1);

				/** calculate mean value of color */
				double imageMean = 0;
				double histogramSum = 0;
				for (int bin = 0; bin < 256; bin++) {
					imageMean += histogram[bin] * bin;
					histogramSum += histogram[bin];
				}
				imageMean /= histogramSum;

				/** draw grayscale histogram */
				barRect.bottom = canvasHeight - 200;
				barRect.left = marginWidth;
				barRect.right = barRect.left + barWidth;
				for (int bin = 0; bin < 256; bin++) {
					float prob = (float) histogram[bin] / (float) histogramSum;
					barRect.top = barRect.bottom
							- Math.min(80, prob * barMaxHeight)
							- barMarginHeight;
					canvas.drawRect(barRect, paintBlack);
					barRect.top += barMarginHeight;
					canvas.drawRect(barRect, paintBlack);
					barRect.left += barWidth;
					barRect.right += barWidth;
				}

			} else {
				/** user wants RGB histogram */
				/** convert image from YUV model to RGB model */
				decodeYUV420SP(RGBData, YUVData, imageWidth, imageHeight);

				double imageRedMean = 0, imageGreenMean = 0, imageBlueMean = 0;
				double redHistogramSum = 0, greenHistogramSum = 0, blueHistogramSum = 0;

				if (color == "RGB") {
					/** Calculate histograms */
					calculateIntensityHistogram(RGBData, redHistogram,
							imageWidth, imageHeight, 1);
					calculateIntensityHistogram(RGBData, greenHistogram,
							imageWidth, imageHeight, 2);
					calculateIntensityHistogram(RGBData, blueHistogram,
							imageWidth, imageHeight, 3);

					/** calculate mean */
					for (int bin = 0; bin < 256; bin++) {
						imageRedMean += redHistogram[bin] * bin;
						redHistogramSum += redHistogram[bin];
						imageGreenMean += greenHistogram[bin] * bin;
						greenHistogramSum += greenHistogram[bin];
						imageBlueMean += blueHistogram[bin] * bin;
						blueHistogramSum += blueHistogram[bin];
					}
					imageRedMean /= redHistogramSum;
					imageGreenMean /= greenHistogramSum;
					imageBlueMean /= blueHistogramSum;

				} else if (color == "red") {
					/** calculate histograms */
					calculateIntensityHistogram(RGBData, redHistogram,
							imageWidth, imageHeight, 1);

					/** calculate mean */
					for (int bin = 0; bin < 256; bin++) {
						imageRedMean += redHistogram[bin] * bin;
						redHistogramSum += redHistogram[bin];
					}
					imageRedMean /= redHistogramSum;

				} else if (color == "green") {
					/** Calculate histograms */
					calculateIntensityHistogram(RGBData, greenHistogram,
							imageWidth, imageHeight, 1);

					/** calculate mean */
					for (int bin = 0; bin < 256; bin++) {
						imageGreenMean += greenHistogram[bin] * bin;
						greenHistogramSum += greenHistogram[bin];
					}
					imageGreenMean /= greenHistogramSum;

				} else if (color == "blue") {
					/** Calculate histograms */
					calculateIntensityHistogram(RGBData, blueHistogram,
							imageWidth, imageHeight, 1);

					/** calculate mean */
					for (int bin = 0; bin < 256; bin++) {
						imageRedMean += blueHistogram[bin] * bin;
						blueHistogramSum += blueHistogram[bin];
					}
					imageBlueMean /= blueHistogramSum;
				}

				/** draw needed histogram(s) */
				if (color == "RGB" || color == "red") {
					/** draw red histogram */
					barRect.bottom = canvasHeight - 300;
					barRect.left = marginWidth;
					barRect.right = barRect.left + barWidth;
					for (int bin = 0; bin < 256; bin++) {
						barRect.top = barRect.bottom
								- Math.min(
										80,
										((float) redHistogram[bin] / (float) redHistogramSum)
												* barMaxHeight)
								- barMarginHeight;
						canvas.drawRect(barRect, paintBlack);
						barRect.top += barMarginHeight;
						canvas.drawRect(barRect, paintRed);
						barRect.left += barWidth;
						barRect.right += barWidth;
					}
				}
				if (color == "RGB" || color == "green") {
					/** draw green histogram */
					barRect.bottom = canvasHeight - 200;
					barRect.left = marginWidth;
					barRect.right = barRect.left + barWidth;
					for (int bin = 0; bin < 256; bin++) {
						barRect.top = barRect.bottom
								- Math.min(80, ((float) greenHistogram[bin])
										/ ((float) greenHistogramSum)
										* barMaxHeight) - barMarginHeight;
						canvas.drawRect(barRect, paintBlack);
						barRect.top += barMarginHeight;
						canvas.drawRect(barRect, paintGreen);
						barRect.left += barWidth;
						barRect.right += barWidth;
					}
				}
				if (color == "RGB" || color == "blue") {
					/** draw blue histogram */
					barRect.bottom = canvasHeight - 100;
					barRect.left = marginWidth;
					barRect.right = barRect.left + barWidth;
					for (int bin = 0; bin < 256; bin++) {
						barRect.top = barRect.bottom
								- Math.min(80, ((float) blueHistogram[bin])
										/ ((float) blueHistogramSum)
										* barMaxHeight) - barMarginHeight;
						canvas.drawRect(barRect, paintBlack);
						barRect.top += barMarginHeight;
						canvas.drawRect(barRect, paintBlue);
						barRect.left += barWidth;
						barRect.right += barWidth;
					}
				}
			}
		}
	}

	/** Conversion from YUV model to RGB model */
	static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width,
			int height) {
		final int frameSize = width * height;

		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0)
					y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}

				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0)
					r = 0;
				else if (r > 262143)
					r = 262143;
				if (g < 0)
					g = 0;
				else if (g > 262143)
					g = 262143;
				if (b < 0)
					b = 0;
				else if (b > 262143)
					b = 262143;

				rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000)
						| ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
			}
		}
	}

	/** Conversion from YUV model to grayscale model */
	static public void decodeYUV420SPGrayscale(int[] rgb, byte[] yuv420sp,
			int width, int height) {
		final int frameSize = width * height;

		for (int pix = 0; pix < frameSize; pix++) {
			int pixVal = (0xff & ((int) yuv420sp[pix])) - 16;
			if (pixVal < 0)
				pixVal = 0;
			if (pixVal > 255)
				pixVal = 255;
			rgb[pix] = 0xff000000 | (pixVal << 16) | (pixVal << 8) | pixVal;
		}
	}

	/** Calculation of intensity histogram for each color channel */
	static public void calculateIntensityHistogram(int[] rgb, int[] histogram,
			int width, int height, int component) {
		/** set all values of histogram to 0 */
		for (int bin = 0; bin < 256; bin++) {
			histogram[bin] = 0;
		}

		if (component == 1) {
			/** red channel */
			for (int pix = 0; pix < width * height; pix += 3) {
				int pixVal = (rgb[pix] >> 16) & 0xff;
				histogram[pixVal]++;
			}
		} else if (component == 2) {
			/** green channel */
			for (int pix = 0; pix < width * height; pix += 3) {
				int pixVal = (rgb[pix] >> 8) & 0xff;
				histogram[pixVal]++;
			}
		} else {
			/** blue channel */
			for (int pix = 0; pix < width * height; pix += 3) {
				int pixVal = rgb[pix] & 0xff;
				histogram[pixVal]++;
			}
		}
	}

}
