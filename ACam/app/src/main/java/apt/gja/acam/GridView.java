package apt.gja.acam;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class GridView extends View {
	
	/** Line for grid */
	private Paint line;
	
	/** Constructors that will allow the View to be instantiated either
	 * in code or through inflation from a resource layout.
	 */
	public GridView(Context context) {
		super(context);
		initGridView();
	}

	public GridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGridView();
	}

	public GridView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initGridView();
	}

	/** Initialize the control and call it from each constructor. */
	protected void initGridView() {
		/** set this view focusable */
		setFocusable(true);

		/** define Paint (line) */
		line = new Paint(Paint.ANTI_ALIAS_FLAG);
		line.setColor(Color.BLACK);
		line.setStrokeWidth(2);		
	}

	/** Draw grid by using prepared line */
	@Override
	protected void onDraw(Canvas canvas) {
		/** find which grid should be drawn */
		MainActivity main_activity = (MainActivity)this.getContext();

		if (main_activity.grid_num == 4) {
			/** draw lines */
			canvas.drawLine(0, getMeasuredHeight()/2, getMeasuredWidth(), getMeasuredHeight()/2, line);
			canvas.drawLine(getMeasuredWidth()/2, 0, getMeasuredWidth()/2, getMeasuredHeight(), line);
		}
		
		if (main_activity.grid_num == 9) {
			/** draw lines */
			canvas.drawLine(0, getMeasuredHeight()/3, getMeasuredWidth(), getMeasuredHeight()/3, line);
			canvas.drawLine(0, (getMeasuredHeight()/3)*2, getMeasuredWidth(), (getMeasuredHeight()/3)*2, line);
			canvas.drawLine(getMeasuredWidth()/3, 0, getMeasuredWidth()/3, getMeasuredHeight(), line);
			canvas.drawLine((getMeasuredWidth()/3)*2, 0, (getMeasuredWidth()/3)*2, getMeasuredHeight(), line);
		}

		canvas.restore();	
	}

}
