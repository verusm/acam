package apt.gja.acam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ZoomControls;

import static android.location.LocationManager.NETWORK_PROVIDER;

public class MainActivity extends Activity {	
	private static final String TAG = "MainActivity";
	public final static String EXTRA_IMAGE_PATH = "apt.gja.acam.IMAGE_PATH";
	
	private CameraView camera;	
	private ZoomControls zoom;
    
    private SeekBar exposure = null;
    private boolean exposure_visible = false;
    
    private int resolution_index = 0;
    private Button b_resolution;
    
    private int focus_index = 0;
    private Button b_focus;

    private int effect_index = 0;
    private Button b_effect;
    
    private DialogFragment focusDialog = null;
    private DialogFragment effectDialog = null;
    private DialogFragment gridDialog = null;
    private DialogFragment histogramDialog = null;

    public int grid_num = 0;
    private GridView grid;
    private DrawHistogramView histogram;

    /** Sensors */
	private SensorManager sensorManager = null;
	private Sensor sensorAccelerometer;
	private Sensor sensorMagneticField;	
	private float[] vAccelerometer = new float[3];
	private float[] vMagneticField = new float[3];  
	private float[] matrixR = new float[9];
	private float[] matrixI = new float[9];
	private float[] matrixValues = new float[9];
	private float azimuth = 0;
	
	private LocationManager locationManager = null;
	private Location locationFix = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG, "onCreate");
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    	/** keep screen active */
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
    	/** set all necessary views */
        camera = (CameraView)findViewById(R.id.camera);	    
        zoom = (ZoomControls) findViewById(R.id.zoom_control);
	    enableZoom();

    	b_resolution = (Button) findViewById(R.id.b_resolution);
    	b_focus = (Button)findViewById(R.id.b_focus);
    	b_effect = (Button)findViewById(R.id.b_effect);

    	/** set initial text for buttons */
    	b_resolution.setText("RESOLUTION");
    	b_focus.setText("AUTO FOCUS");
    	b_effect.setText("EFFECT");
    	
    	grid = (GridView)findViewById(R.id.grid);
    	histogram = (DrawHistogramView)findViewById(R.id.histogram);

    	/** initialize dialogs */
	    focusDialog = new FocusDialogFragment();
	    effectDialog = new EffectDialogFragment();
	    gridDialog = new GridDialogFragment();
	    histogramDialog = new HistogramDialogFragment();
	    
	    /** sensors */
	    sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);	    
	    if( sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null ) {	
	    	sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    }
	    if( sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null ) {
	    	sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    }
	    
	    /** location */
	    locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 
	    //setLocationProvider();
	    
	    Log.d(TAG, "onCreate ends");
    }
    
    /** LOCATION */
	/** Set location provider which is enable */
	/*private void setLocationProvider(){
		boolean network = false;
		boolean gps = false;
		
		network = checkInternetConnection((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
		gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		
		locationManager.removeUpdates(onLocationChange);
		// set location providers
		if (gps && network){
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*10, 2, onLocationChange);
			locationManager.requestLocationUpdates(NETWORK_PROVIDER, 1000*10, 2, onLocationChange);
		} else if (gps){
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*10, 2, onLocationChange);
		} else if (network){
			locationManager.requestLocationUpdates(NETWORK_PROVIDER, 1000*10, 2, onLocationChange);
		} else {
			Log.d(TAG, "No GPS, No network");
		}
	}*/
	
	/** Check the Internet connection */
	public static boolean checkInternetConnection(ConnectivityManager conMgr) {
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}
    
	/** Set listener for location */
	private LocationListener onLocationChange = new LocationListener() {
		@Override
		public void onLocationChanged(Location fix) {
			locationFix = fix;
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			
		}
	};
	
    /** SENSORS */    
	/** Set sensor event listener */
	private final SensorEventListener sensorEventListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {

			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
				vAccelerometer = event.values;				
				for(int i=0; i<3; i++){
					vAccelerometer[i] =  event.values[i];
		        }
			}
			if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
				vMagneticField = event.values; 
				for(int i=0; i<3; i++){
					vMagneticField[i] =  event.values[i];	
		        }
			}
			
			updateOrientation();
		}
		
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			
		}		
	};

	/** Update data from sensors */
	private void updateOrientation(){		
		boolean success = SensorManager.getRotationMatrix(
			       matrixR,
			       matrixI,
			       vAccelerometer,
			       vMagneticField);

		if (success){
			SensorManager.getOrientation(matrixR, matrixValues);
			matrixValues[0] = (float) Math.toDegrees(matrixValues[0]);
			matrixValues[1] = (float) Math.toDegrees(matrixValues[1]);
			matrixValues[2] = (float) Math.toDegrees(matrixValues[2]);

			azimuth = matrixValues[0];
		}
	}

    /** EXPOSURE */
    public void clickedExposure(View v) {
    	Log.d(TAG, "clickedExposure");
    	
    	exposure = (SeekBar) findViewById(R.id.seekbar_exposure);
	    exposure.setMax(camera.getMaxExposure() - camera.getMinExposure());
	    /** set exposure to 0 */
	    exposure.setProgress(camera.getCurrentExposure() - camera.getMinExposure());
    	
    	if (!exposure_visible) {
    		exposure.setVisibility(View.VISIBLE);
    		exposure_visible = true;
    	} else {
    		exposure.setVisibility(View.GONE);
    		exposure_visible = false;
    	}
    	
    	if (camera.getCameraObject() != null){

    		exposure.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
    			@Override
    			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    				    				
    			}
    			@Override
    			public void onStartTrackingTouch(SeekBar seekBar) {
    				
    			}
    			@Override
    			public void onStopTrackingTouch(SeekBar seekBar) {
    				Log.d(TAG, "progress: " + seekBar.getProgress());
    				camera.changeExposure(seekBar.getProgress());
    			}
    		});     
    		    
    	}
    }
    
    /** RESOLUTION */
    public void clickedResolution(View v) {
    	Log.d(TAG, "clickedResolution");

    	resolution_index++;
    	resolution_index = resolution_index % camera.getSupportedResolutions().size();
    	setResolution();
    }
    
    private void setResolution() {
    	/** set text in button and camera parameter */
    	b_resolution.setText(String.valueOf(camera.getSupportedResolutions().get(resolution_index).width) + "x" + String.valueOf(camera.getSupportedResolutions().get(resolution_index).height));
    	camera.setResolution(camera.getSupportedResolutions().get(resolution_index).width, camera.getSupportedResolutions().get(resolution_index).height);
    }
    
    /** FOCUS */
    public void clickedFocus(View v) {
    	Log.d(TAG, "clickedFocus");       	
    	Log.d(TAG, "current focus mode: " + camera.getCurrentFocusMode()); 

    	focusDialog.show(getFragmentManager(), "focus_dialog");
    }
    
    public void clickedFocusAuto(View v) {
    	Log.d(TAG, "clickedFocusAuto");
 
    	focus_index = 0;
    	camera.setFocus(Camera.Parameters.FOCUS_MODE_AUTO);
    	b_focus.setText("AUTO FOCUS");
    	focusDialog.dismiss();
    }
    
    public void clickedFocusInfinity(View v) {
    	Log.d(TAG, "clickedFocusInfinity");
    	
    	focus_index = 1;
    	camera.setFocus(Camera.Parameters.FOCUS_MODE_INFINITY);
    	b_focus.setText("Infinity");
    	focusDialog.dismiss();
    }

    public void clickedFocusMacro(View v) {
    	Log.d(TAG, "clickedFocusMacro");
    	
    	focus_index = 2;
    	camera.setFocus(Camera.Parameters.FOCUS_MODE_MACRO);
    	b_focus.setText("Macro");	
    	focusDialog.dismiss();
    }
    
    /** IMAGE EFFECTS */
    public void clickedEffect(View v) {
    	Log.d(TAG, "clickedEffect");
    	
    	effectDialog.show(getFragmentManager(), "effect_dialog");
    }
    
    public void clickedEffectNone(View v) {
    	Log.d(TAG, "clickedEffectNone");
    	
    	effect_index = 0;
    	camera.setEffect(Camera.Parameters.EFFECT_NONE);
    	b_effect.setText("EFFECT");
    	
    	effectDialog.dismiss();    	
    }
    
    public void clickedEffectMono(View v) {
    	Log.d(TAG, "clickedEffectMono");
    	
    	effect_index = 1;
    	camera.setEffect(Camera.Parameters.EFFECT_MONO);
    	b_effect.setText("Mono");
    	
    	effectDialog.dismiss();    	
    }
    
    public void clickedEffectNegative(View v) {
    	Log.d(TAG, "clickedEffectNegative");
    	
    	effect_index = 2;
    	camera.setEffect(Camera.Parameters.EFFECT_NEGATIVE);
    	b_effect.setText("Negative");
    	
    	effectDialog.dismiss();    	
    }
    
    public void clickedEffectSepia(View v) {
    	Log.d(TAG, "clickedEffectSepia");
    	
    	effect_index = 3;
    	camera.setEffect(Camera.Parameters.EFFECT_SEPIA);
    	b_effect.setText("Sepia");
    	
    	effectDialog.dismiss();    	
    }
       
    /** GRID */
    public void clickedGrid(View v) {
    	Log.d(TAG, "clickedGrid");
    	
    	gridDialog.show(getFragmentManager(), "grid_dialog");
    }
    
    public void clickedGridNone(View v) {
    	Log.d(TAG, "clickedGridNone");
    	
    	grid.setVisibility(View.GONE);    	
    	gridDialog.dismiss();
    }
    
    public void clickedGrid4(View v) {
    	Log.d(TAG, "clickedGrid4");
    	
    	grid.setVisibility(View.GONE);
    	grid_num = 4;
    	grid.setVisibility(View.VISIBLE);
    	gridDialog.dismiss();
    }
    
    public void clickedGrid9(View v) {
    	Log.d(TAG, "clickedGrid9");
    	
    	grid.setVisibility(View.GONE);
    	grid_num = 9;
    	grid.setVisibility(View.VISIBLE);
    	gridDialog.dismiss();
    }
    
    /** HISTOGRAM */
    /** Return histogram view */
    public DrawHistogramView getHistogram() {
    	return histogram;
    }
    
    public void clickedHistogram(View v) {
    	Log.d(TAG, "clickedHistogram");

    	camera.stopPreviewCallback();
    	histogramDialog.show(getFragmentManager(), "histogram_dialog");    	
    }
    
    public void clickedHistogramNone(View v) {
    	Log.d(TAG, "clickedHistogramNone");
    	
    	camera.stopPreviewCallback();
    	histogram.setVisibility(View.GONE);  
    	histogramDialog.dismiss();
    }
    
    public void clickedHistogramRGB(View v) {
    	Log.d(TAG, "clickedHistogramRGB");
    	
    	histogram.setVisibility(View.GONE);
    	camera.startPreviewCallback("RGB");    	
    	histogram.setVisibility(View.VISIBLE); 	
    	histogramDialog.dismiss();
    }
    
    public void clickedHistogramGray(View v) {
    	Log.d(TAG, "clickedHistogramGray");
    	
    	histogram.setVisibility(View.GONE);
    	camera.startPreviewCallback("gray");    	
    	histogram.setVisibility(View.VISIBLE); 	
    	histogramDialog.dismiss();
    }
    
    public void clickedHistogramRed(View v) {
    	Log.d(TAG, "clickedHistogramRed");
    	
    	histogram.setVisibility(View.GONE);
    	camera.startPreviewCallback("red");    	
    	histogram.setVisibility(View.VISIBLE);
    	histogramDialog.dismiss();
    }
    
    public void clickedHistogramGreen(View v) {
    	Log.d(TAG, "clickedHistogramGreen");
    	
    	histogram.setVisibility(View.GONE);
    	camera.startPreviewCallback("green");    	
    	histogram.setVisibility(View.VISIBLE); 	
    	histogramDialog.dismiss();
    }
    
    public void clickedHistogramBlue(View v) {
    	Log.d(TAG, "clickedHistogramBlue");
    	
    	histogram.setVisibility(View.GONE);
    	camera.startPreviewCallback("blue");    	
    	histogram.setVisibility(View.VISIBLE); 	
    	histogramDialog.dismiss();
    }
    
    /** ZOOM */
    private void enableZoom() {
        zoom.setIsZoomInEnabled(true);
        zoom.setIsZoomOutEnabled(true);
        zoom.setOnZoomInClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                zoomCamera(true);
            }
        });
        zoom.setOnZoomOutClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                zoomCamera(false);
            }
        });
    }
    
    /** 
     * Enables zoom feature in camera.
     * @param zoomInOrOut  "false" for zoom in and "true" for zoom out
     */
    public void zoomCamera(boolean zoomInOrOut) {
    	Parameters parameter = camera.getCameraObject().getParameters();

            if(parameter.isZoomSupported()) {
                int MAX_ZOOM = parameter.getMaxZoom();
                int currentZoom = parameter.getZoom();
                    if(zoomInOrOut && (currentZoom <MAX_ZOOM && currentZoom >=0)) {
                        parameter.setZoom(++currentZoom);
                    }
                    else if(!zoomInOrOut && (currentZoom <=MAX_ZOOM && currentZoom >0)) {
                    parameter.setZoom(--currentZoom);
                    }
            }
            else
                Toast.makeText(getApplicationContext(), "Zoom is not avaliable", Toast.LENGTH_SHORT).show();

            camera.getCameraObject().setParameters(parameter);   
    }   

    /** TAKE PICTURE */
    public PictureCallback mPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
        	/** file for picture */
            File pictureFile = getOutputMediaFile();
            
            if (pictureFile == null){
                Log.d(TAG, "Error creating media file, check storage permissions ");
                return;
            } else {
            	Log.d(TAG, "pictureFile: " + pictureFile.getAbsolutePath());
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();   
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }

            /** location fix which should be saved into EXIF data */
            // Format is "num1/denom1,num2/denom2,num3/denom3".
            double latitude = 0;
            double longitude = 0;
            if (locationFix != null) {
            	latitude = locationFix.getLatitude();
            	longitude = locationFix.getLongitude();
            }
            
            Log.d(TAG, "Latitude: " + latitude);
            Log.d(TAG, "Longitude: " + longitude);

            try {
            	/** save EXIF data */
            	ExifInterface exif = new ExifInterface(pictureFile.getAbsolutePath());
                int lat1 = (int)Math.floor(latitude);
                int lat2 = (int)Math.floor((latitude - lat1) * 60);
                double lat3 = (latitude - ((double)lat1+((double)lat2/60))) * 3600000;

                int lon1 = (int)Math.floor(longitude);
                int lon2 = (int)Math.floor((longitude - lon1) * 60);
                double lon3 = (longitude - ((double)lon1+((double)lon2/60))) * 3600000;

                /** save latitude and longitude into EXIF */
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, lat1 + "/1," + lat2 + "/1," + lat3 + "/1000");
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, lon1 + "/1," + lon2 +"/1," + lon3 + "/1000");

                if (latitude > 0) {
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N"); 
                } else {
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
                }

                if (longitude > 0) {
                    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");    
                } else {
                	exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
                }
            	
                /** save azimuth into EXIF */
            	exif.setAttribute("UserComment", "Azimuth: " + String.valueOf(azimuth));
            	exif.saveAttributes();
            	
            } catch (IOException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
            
            /** start new activity with image which was taken */
            startNextActivity(pictureFile.getAbsolutePath());
        }
    };

    /** Create a File for saving an image */
    private  File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                  Environment.DIRECTORY_DCIM), "ACam");

        /** Create the directory if it does not exist */
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        /** Create the file name */
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
        
        return mediaFile;
    }
    
    /** start new activity - ViewImageActivity class */
	private void startNextActivity(String path){
	    Intent intent = new Intent(this, ViewImageActivity.class);
	    
	    intent.putExtra(EXTRA_IMAGE_PATH, path);
	    startActivity(intent);
	}
    
	/** Set parameters if it goes back to main activity */
    private void setPreviousParams() {
    	/** set resolution */
    	camera.setResolution(camera.getSupportedResolutions().get(resolution_index).width, camera.getSupportedResolutions().get(resolution_index).height);
    	String text = String.valueOf(camera.getSupportedResolutions().get(resolution_index).width) + "x" + String.valueOf(camera.getSupportedResolutions().get(resolution_index).height);
    	b_resolution.setText(text);
    	
    	/** set focus */
    	if (focus_index == 0) {
    		camera.setFocus(Camera.Parameters.FOCUS_MODE_AUTO);
    	} else if (focus_index == 1) {
    		camera.setFocus(Camera.Parameters.FOCUS_MODE_INFINITY);
    	} else if (focus_index == 2) {
    		camera.setFocus(Camera.Parameters.FOCUS_MODE_MACRO);
    	}
    	
    	/** set effect */
    	if (effect_index == 0) {
    		camera.setEffect(Camera.Parameters.EFFECT_NONE);
    	} else if (effect_index == 1) {
    		camera.setEffect(Camera.Parameters.EFFECT_MONO);
    	} else if (effect_index == 2) {
    		camera.setEffect(Camera.Parameters.EFFECT_NEGATIVE);
    	} else if (effect_index == 3) {
    		camera.setEffect(Camera.Parameters.EFFECT_SEPIA);
    	}    	
    }

	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
		camera.releaseCamera();
		
		sensorManager.unregisterListener(sensorEventListener, sensorAccelerometer);
		sensorManager.unregisterListener(sensorEventListener, sensorMagneticField);
		locationManager.removeUpdates(onLocationChange);

		Log.d(TAG, "onPause ends");
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		sensorManager.registerListener(sensorEventListener, sensorAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
		sensorManager.registerListener(sensorEventListener, sensorMagneticField, SensorManager.SENSOR_DELAY_FASTEST);
		//setLocationProvider();

		Log.d(TAG, "onResume ends");
	}
	
	@Override
	public void onRestart() {
		Log.d(TAG, "onRestart");
		super.onRestart();	
		Log.d(TAG, "grid_num: " + grid_num);

		camera.openCamera();
		setPreviousParams();
		Log.d(TAG, "onRestart ends");
	}

}
