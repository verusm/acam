package apt.gja.acam;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class ViewImageActivity extends Activity {	
	//private static final String TAG = "ViewImageActivity";
	
	private ImageView image;
	private String path = null;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_image);
		
		/** read file path from main activity */
		Intent intent = getIntent();
		path = intent.getStringExtra(MainActivity.EXTRA_IMAGE_PATH);
		
		Toast.makeText(getApplicationContext(), "Image was saved into " + path + " file.", Toast.LENGTH_SHORT).show();
		
		/** initialize bitmap for picture */
		Bitmap bitmap = BitmapFactory.decodeFile(path);
		
		/** Scale-down */
		DisplayMetrics metrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(metrics);
	    
	    int maxValue = Math.max(metrics.heightPixels, metrics.widthPixels);
		
		if(bitmap.getHeight()>=metrics.heightPixels||bitmap.getWidth()>=metrics.widthPixels){
		    float ratio = Math.min(
		            (float) maxValue / bitmap.getWidth(),
		            (float) maxValue / bitmap.getHeight());
		    int width = (int) (ratio * bitmap.getWidth());
		    int height = (int) (ratio * bitmap.getHeight());
	
		    bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
		}	

		/** set image */
		image = (ImageView) findViewById(R.id.image_view);
		image.setImageBitmap(bitmap);
	}
	
	/** button OK */
	public void clickedOk(View v) {
		/** finish this activity */
		ViewImageActivity.this.finish();
    	return;
	}
	
	/** button DELETE */
	public void clickedDelete(View v) {
		/** delete photo file and finish this activity */
		File file = new File(path);
		boolean deleted = file.delete();
		if (deleted) {
			Toast.makeText(getApplicationContext(), "Image " + path + " was successfully removed from SD card.", Toast.LENGTH_SHORT).show();
			ViewImageActivity.this.finish();
		} else {
			Toast.makeText(getApplicationContext(), "ERROR during deleting image from SD card.", Toast.LENGTH_SHORT).show();
			ViewImageActivity.this.finish();
		}
	}
	
	@Override
	public void onRestart(){
		super.onRestart();
	}
}